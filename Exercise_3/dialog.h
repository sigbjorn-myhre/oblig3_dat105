#ifndef DIALOG_H
#define DIALOG_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

class Dialog : public QWidget {
    Q_OBJECT

    QHBoxLayout *hbox;
    QLabel *label;
    QPushButton *button;

public:
    explicit Dialog(QWidget *parent = 0, std::string = "");

signals:

public slots:
};

#endif // DIALOG_H
