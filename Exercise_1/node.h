#ifndef NODE_H
#define NODE_H
#include <vector>
#include<climits>
#include<cstdlib>
class Edge;

class Node {
public:
    Node(int d) : data(d), visited(false), distance(INT_MAX), previous(NULL){}
    char data;
    bool visited;
    int distance;
    Node* previous;
    std::vector<Edge*> edgeList;
};

#endif // NODE_H
