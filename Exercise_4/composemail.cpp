#include "composemail.h"

ComposeMail::ComposeMail(QWidget *parent) : QWidget(parent) {
    // mainVBox and window
    this->mainVBox = new QVBoxLayout(this);
    this->setLayout(this->mainVBox);
    this->setGeometry(0, 0, 600, 450);
    this->setWindowTitle("Compose mail");

    // hboxes
    this->hbox1 = new QHBoxLayout();
    this->hbox2 = new QHBoxLayout();

    // vboxes
    this->vbox1 = new QVBoxLayout();
    this->vbox2 = new QVBoxLayout();

    // Labels
    this->toLabel = new QLabel("To:", this);
    vbox1->addWidget(this->toLabel);
    this->subjectLabel = new QLabel("Subject:", this);
    vbox1->addWidget(this->subjectLabel);
    hbox1->addLayout(vbox1);

    // LineEdits
    this->toLineEdit = new QLineEdit(this);
    vbox2->addWidget(this->toLineEdit);
    this->subjectLineEdit = new QLineEdit(this);
    vbox2->addWidget(this->subjectLineEdit);
    hbox1->addLayout(vbox2);

    // TextEdit
    this->textEdit = new QTextEdit(this);

    // Buttons
    this->sendBtn = new QPushButton("&Send", this);
    this->cancelBtn = new QPushButton("&Cancel", this);
    hbox2->addWidget(this->sendBtn);
    hbox2->addWidget(this->cancelBtn);
    hbox2->addStretch();
    connect(this->sendBtn, SIGNAL(clicked(bool)), this, SLOT(sendBtnClicked()));
    connect(this->cancelBtn, SIGNAL(clicked(bool)), this, SLOT(cancelBtnClicked()));

    // Add layouts
    this->mainVBox->addLayout(hbox1);
    this->mainVBox->addWidget(this->textEdit);
    this->mainVBox->addLayout(hbox2);
}

void ComposeMail::sendBtnClicked() {
    qDebug("Send-button clicked");
}

void ComposeMail::cancelBtnClicked() {
    qDebug("Cancel-button clicked");
    this->close();
}
