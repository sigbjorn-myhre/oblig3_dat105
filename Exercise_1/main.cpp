#include <iostream>
#include "priorityqueue.h"
#include "edge.h"
#include "node.h"
using namespace std;

int main(void) {
    PriorityQueue pq;

    Node *a = new Node('a');
    Node *b = new Node('b');
    Node *c = new Node('c');
    Node *d = new Node('d');

    Edge *w = new Edge(d, a, 8);
    Edge *x = new Edge(a, b, 5);
    Edge *y = new Edge(b, c, 7);
    Edge *z = new Edge(c, d, 3);

    pq.add(x);
    pq.add(y);
    pq += *z;
    pq += *w;

    pq.printQueue();

    cout << "Hentet " << pq.remove() << " fra køen." << endl;
    pq.printQueue();

    return 0;
}
