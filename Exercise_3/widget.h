#ifndef WIDGET_H
#define WIDGET_H

#include <vector>
#include <random>

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QSignalMapper>

#include "dialog.h"

class Widget : public QWidget {
    Q_OBJECT

    QHBoxLayout *hbox;
    QPushButton *newGameBtn;
    QSignalMapper *m;

    std::vector<QPushButton*> buttons;
    int count = 10; // Antall knapper i spillet. Kan endres

    int guessValue;

    Dialog *dialogBox;

    void incorrect(int, int);
    void correct(int);
    void generateRandom();

public:
    Widget(QWidget *parent = 0);
    ~Widget();

public slots:
    void guess(int);
    void newGame();
};

#endif // WIDGET_H
