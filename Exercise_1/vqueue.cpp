#include "vqueue.h"

// Konstruktør
template <typename T>
vQueue<T>::vQueue() {
    this->sz = 0;
}

// Legger element bakerst
template <typename T>
void vQueue<T>::enqueue(T elem) {
    this->queue.push_back(elem);
    this->sz++;
}

// Henter ut fremste element
template <typename T>
T vQueue<T>::dequeue() {
    T r = {}; // NULL

    if(!isEmpty()) {
        r = this->queue[0];
        shiftElements();
        this->sz--;
    }

    return r;
}

// Flytter alle element ett hakk fram
template <typename T>
void vQueue<T>::shiftElements() {
    for(int i = 0; i < this->sz - 1; i++) {
        this->queue[i] = this->queue[i+1];
    }
}

// Sjekker om køen er tom
template <typename T>
bool vQueue<T>::isEmpty() {
    return this->sz <= 0;
}

//
template class vQueue<std::string>;
template class vQueue<int>;
