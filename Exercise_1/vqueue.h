#ifndef VQUEUE_H
#define VQUEUE_H

#include <vector>
#include <iostream>

template <typename T>
class vQueue {
    std::vector<T> queue;
    int sz;

    void shiftElements();
public:
    vQueue();
    void enqueue(T);
    T dequeue();
    bool isEmpty();
};

#endif // VQUEUE_H
