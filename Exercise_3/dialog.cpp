#include "dialog.h"

Dialog::Dialog(QWidget *parent, std::string mes) : QWidget(parent) {
    this->label = new QLabel(this);
    this->label->setText(QString::fromStdString(mes));

    this->button = new QPushButton("&Ok", this);
    connect(button, SIGNAL(clicked(bool)), this, SLOT(close()));

    this->hbox = new QHBoxLayout(this);
    this->hbox->addWidget(this->label);
    this->hbox->addWidget(this->button);
}
