#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include "edge.h"
#include "node.h"
#include <vector>
#include <iostream>

class PriorityQueue {
    std::vector<Edge*> queue;
    int sz;

    void shiftBack(int);
    void shiftForward();
public:
    PriorityQueue();
    void add(Edge*);
    Edge* remove();

    bool isEmpty();
    void printQueue();

    PriorityQueue& operator+=(Edge&);
};

#endif // PRIORITYQUEUE_H
