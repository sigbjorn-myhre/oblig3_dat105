#include "edge.h"

Edge::Edge(Node* v, Node* w, int wt, bool d){
    endpoint[0] = v;
    endpoint[1] = w;
    weight = wt;
    isDirected = d;
}

std::ostream& operator<<(std::ostream& os, const Edge* e) {
    os << "(" << e->endpoint[0]->data << ", " << e->endpoint[1]->data << ", " << e->weight << ")";
    return os;
}
