#include "widget.h"

// Konstruktør. Oppretter og connecter widgets
Widget::Widget(QWidget *parent) : QWidget(parent) {
    // Opprett knapper med tall i label
    for(int i = 0; i < this->count; i++) {
        buttons.push_back(new QPushButton(QString::number(i), this));
    }

    // Opprett hbox
    this->hbox = new QHBoxLayout(this);

    // Legg knapper i hbox
    for(auto b : buttons) {
        this->hbox->addWidget(b);
    }

    // New game-knapp
    this->newGameBtn = new QPushButton("&New game", this);
    this->hbox->addWidget(this->newGameBtn);
    connect(this->newGameBtn, SIGNAL(clicked(bool)), this, SLOT(newGame()));

    // Guess value
    this->generateRandom();

    // Signal-mapper
    this->m = new QSignalMapper(this);

    // Bind buttons to signal-mapper
    for(QPushButton *b : buttons) {
        connect(b, SIGNAL(clicked(bool)), m, SLOT(map()));
        m->setMapping(b, b->text().toInt());
    }
    // and bind mapper to guess-slot
    connect(m, SIGNAL(mapped(int)), this, SLOT(guess(int)));

    // Dialogbox
    this->dialogBox = new Dialog(0, "You won! Congratulations!");
    this->dialogBox->hide();

    // Before game starts, buttons are disabled
    for(QPushButton *b : buttons) {
        b->setEnabled(false);
    }
}

Widget::~Widget() {  }

// Håndterer hvilken knapp som blir trykt, sjekker om det er korrekt eller ikke
void Widget::guess(int guessedValue) {
    if(guessedValue < this->guessValue) {
        this->incorrect(0, guessedValue);
    }
    else if (guessedValue > this->guessValue) {
        this->incorrect(guessedValue, this->count-1);
    }
    else {
        this->correct(guessedValue);
    }
}

// Starter nytt spill
void Widget::newGame() {
    for(QPushButton *b : this->buttons) {
        b->setStyleSheet(""); // Resets stylesheet
        b->setEnabled(true);
    }

    // Guess value
    this->generateRandom();
}

// Farger knappene røde
void Widget::incorrect(int from, int to) {
    for(int i = from; i <= to; i++) {
        this->buttons[i]->setStyleSheet("* { background-color: red } ");
        this->buttons[i]->setEnabled(false);
    }
}

// Gjettet korrekt. Spillet avsluttes
void Widget::correct(int value) {
    this->buttons[value]->setStyleSheet("* { background-color: green } ");

    // Disable the rest of the buttons
    for(QPushButton *b : this->buttons) {
        b->setEnabled(false);
    }

    this->dialogBox->show();
}

// Genererer ny tilfeldig verdi for gjetting
void Widget::generateRandom() {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0, this->count-1);
    this->guessValue = dist(mt);
}
