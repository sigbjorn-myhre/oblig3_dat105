#include "widget.h"

Widget::Widget(QWidget *parent) : QWidget(parent) {
    // hbox and window
    this->hbox = new QHBoxLayout(this);
    this->setLayout(this->hbox);
    this->setGeometry(0, 0, 600, 350);
    this->setWindowTitle("My Little Mail Client");

    // Buttons
    this->checkBtn = new QPushButton("&Check", this);
    this->deleteBtn = new QPushButton("&Delete", this);
    this->newBtn = new QPushButton("&New...", this);
    connect(this->checkBtn, SIGNAL(clicked(bool)), this, SLOT(checkBtnClicked()));
    connect(this->deleteBtn, SIGNAL(clicked(bool)), this, SLOT(deleteBtnClicked()));
    connect(this->newBtn, SIGNAL(clicked(bool)), this, SLOT(newBtnClicked()));

    // vbox for buttons
    this->vbox = new QVBoxLayout();
    this->vbox->addWidget(this->checkBtn);
    this->vbox->addWidget(this->deleteBtn);
    this->vbox->addWidget(this->newBtn);
    this->vbox->addStretch();

    // Listview
    this->listView = new QListView(this);
    this->stringListModel = new QStringListModel(this);
    QStringList list;
    list << "Per Hallo!! Hei! Hvordan går det?";
    list << "Kari Hjelp Trenger litt hjelp med oblig 2";
    this->stringListModel->setStringList(list);
    //this->stringListModel->setStringList(*stringList); //???
    this->listView->setModel(this->stringListModel);

    // Add items to hbox
    this->hbox->addWidget(this->listView);
    this->hbox->addLayout(this->vbox);
}

Widget::~Widget() { }

void Widget::checkBtnClicked() {
    qDebug("Check-button clicked");
}

void Widget::deleteBtnClicked() {
    qDebug("Delete-button clicked");
}

void Widget::newBtnClicked() {
    qDebug("New-button clicked");
    // Oppretter nytt composeMail-vindu
    this->composeWindow = new ComposeMail();
    this->composeWindow->show();
}
